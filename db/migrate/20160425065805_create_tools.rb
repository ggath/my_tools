class CreateTools < ActiveRecord::Migration
  def change
    create_table :tools do |t|
      t.string :name
      t.belongs_to :category, index: true, foreign_key: true
      t.text :description
      t.string :url

      t.timestamps null: false
    end
  end
end
