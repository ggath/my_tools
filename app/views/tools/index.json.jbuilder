json.array!(@tools) do |tool|
  json.extract! tool, :id, :name, :category_id, :description, :url
  json.url tool_url(tool, format: :json)
end
